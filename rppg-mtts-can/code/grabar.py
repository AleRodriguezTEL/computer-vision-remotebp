import cv2
import time
if __name__ == "__main__":
    ## opening videocapture
    cap = cv2.VideoCapture(0)

    ## some videowriter props
    sz = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    fps = 30
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')


    # open and set props
    start_point = (int(sz[0]*0.3), int(sz[1]*0.3))
    end_point = (int(sz[0]*0.7), int(sz[1]*0.76))
    vid_sz = (end_point[0] - start_point[0], end_point[1] - start_point[1])
    vout = cv2.VideoWriter()
    vout.open('output.mp4',fourcc,fps,vid_sz,True)

    cnt = 0
    while cnt < 246:
        cnt += 1
        ret, frame = cap.read()
        frame = cv2.flip(frame, 1)
        crop_img = frame[start_point[1]:end_point[1], start_point[0]:end_point[0]]
        vout.write(crop_img)
        
        cv2.putText(frame, str(cnt), (10, 20), cv2.FONT_HERSHEY_PLAIN, 1, (0,255,0), 1, cv2.LINE_AA)
        cv2.rectangle(frame, start_point, end_point, (255, 0, 0), 2)
        cv2.imshow('asd', frame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        

    vout.release()
    cap.release()
    cv2.destroyAllWindows()

