## rPPG con MTTS-CAN

Obtención de señal rPPG a partir de un video del rostro.

Paper: https://papers.nips.cc/paper/2020/file/e1228be46de6a0234ac22ded31417bc7-Paper.pdf

Git original: https://github.com/xliucs/MTTS-CAN 

## Instalación de ambiente:

* Importar el ambiente desde el .yml

```
$ conda env create -f [environment.yml]
```

* Descargar el modelo en la carpeta principal del proyecto.

## Ejecución 

1. Predicción de  señal rPGG a partir de un video . 

``` 
$ bash extract_signals @video_dir @signal_dir @sample_len
```
- video_dir: ruta a la carpeta donde se encuentran los videos a utilizar.  
- signal_dir: ruta a la carpeta donde se guardaran los resultados en archivos .p.
- sample_len: tamaño de la señal resultado. El módulo RPPG2ABP utiliza un valor de 1024 y las redes del proyecto [Non-invasive]() utilizan un valor de 875.

## Modelos y otros archivos
Los archivos adicionales se pueden encontrar en este drive:
https://drive.google.com/drive/folders/12Kp15hFE2OwhHELRtYOh8QCM2Je07kVC?usp=sharing
