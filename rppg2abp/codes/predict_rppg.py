#Deshabilitar warnings
import logging
import os
import warnings
warnings.filterwarnings("ignore", message=r"Passing", category=FutureWarning)
logging.getLogger('tensorflow').disabled = True
os.environ["KMP_AFFINITY"] = "noverbose"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf
import pandas as pd
from numpy import newaxis
from helper_functions import *
from models import UNetDS64, MultiResUNet1D
from predict_test import predict_test_data
import argparse
from scipy.signal import find_peaks


#Deshabilitar GPU
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

#función predecir
def predict(args):


    length = 1024               # length of signal
    #dir = os.getcwd() +'\\'  #Directory for WINDOWS 
    #Cargar parámetros
    print("Cargando parámetros ...")
    dt = pickle.load(open(os.path.join('data', 'meta9.p'), 'rb'))	    # loading metadata LINUX
    #dt = pickle.load(open(os.path.join(dir+'data', 'meta9.p'), 'rb'))			# loading metadata WINDOWS
    max_ppg = dt['max_ppg']
    min_ppg = dt['min_ppg']
    max_abp = dt['max_abp']
    min_abp = dt['min_abp']
    print("...Completo")
    #cargar modelos
    print("Cargando modelos ... ")
    mdl1 = UNetDS64(length)                                             # creating approximation network
    mdl1.load_weights(os.path.join('models','ApproximateNetwork.h5'))   # loading weights

    mdl2 = MultiResUNet1D(length)                                       # creating refinement network
    mdl2.load_weights(os.path.join('models','RefinementNetwork.h5'))    # loading weights
    print("...Completo")
    #while True:
        
    #data_path = input('Ingrese el archivo a inferir. Ej: "data/dataset-yarppg/01.p" .\nPresione enter para salir\n> ')


    #data_path = dir + 'data\\' + args.method + "\ppg_prediction.p"  #WINDOWS
    #signal_path = "data/"+args.method + "/ppg_prediction.p"

    signal_path = args.signal_file
    #print("Usando data de "+ 'data/' + args.method)
    
    if(signal_path == ""):
        #break
        print("ERROR: Data not found")
        exit(1)
    try:

        #cargar data
        print("Cargando data ... ")
        dt = pickle.load(open(signal_path,'rb'))      # loading test data
        X_test = dt[np.newaxis, ..., np.newaxis]
        
        #inferencia
        print("Realizando inferencia ...")
        Y_test_pred_approximate = mdl1.predict(X_test,verbose=1)            # predicting approximate abp waveform
        Y_test_pred = mdl2.predict(Y_test_pred_approximate[0],verbose=1)    # predicting abp waveform

        #guardar data
        print("Guardando datos ...")
        pickle.dump(Y_test_pred_approximate,open('test_output_approximate.p','wb')) # saving the approxmiate predictions
        pickle.dump(Y_test_pred,open('test_output.p','wb'))                 # saving the predicted abp waeforms


        #plot
        print("Graficando ... ")
        

    
    


        ppg_signal = X_test[0] * max_ppg + min_ppg											# input ppg signal
        abp_signal_pred_approximate = Y_test_pred_approximate[0][0] * max_abp + min_abp		# abp waveform approx.
        abp_signal_pred = Y_test_pred[0] * max_abp + min_abp

        #obtener peaks y valles a una distancia de 64 muestras (2Hz)
        peaks, _ = find_peaks(abp_signal_pred[:,0], distance=64)
        valles, _ = find_peaks(-abp_signal_pred[:,0], distance=64)

        #filtrar peaks a una distancia de la media mayor a un threshold
        threshold_peaks = 40
        media_peaks = 0
        for peak in peaks:
            media_peaks += abp_signal_pred[:,0][peak]
        media_peaks = media_peaks/len(peaks)
        peaks_filtrados = list()
        for peak in peaks:
            if abs(abp_signal_pred[:,0][peak] - media_peaks) <= threshold_peaks:
                peaks_filtrados.append(peak)
        peaks_filtrados = np.array(peaks_filtrados)

        #filtrar peaks a una distancia de la media mayor a un threshold
        threshold_valles = 20
        media_valles = 0
        for valle in valles:
            media_valles += abp_signal_pred[:,0][valle]
        media_valles = media_valles/len(valles)
        valles_filtrados = list()
        for valle in valles:
            if abs(abp_signal_pred[:,0][valle] - media_valles) <= threshold_valles:
                valles_filtrados.append(valle)
        valles_filtrados = np.array(valles_filtrados)

        #obtener la media de los peak denuevo
        media = 0
        for peak in peaks_filtrados:
            media += abp_signal_pred[:,0][peak]
        media = media/len(peaks_filtrados)
        print("Predicción de SBP: "+str(media))
        SBP = media
        #obtener la media de los valles denuevo
        media = 0
        for valle in valles_filtrados:
            media += abp_signal_pred[:,0][valle]
        media = media/len(valles_filtrados)
        print("Predicción de DBP: "+str(media))
        DBP = media
        time_scale = np.arange(0, 8.192, 8.192/len(ppg_signal))	
        time_scale = time_scale[..., np.newaxis]						# series for time axis

        if(args.data_path!=""):

            #Saving data in the DATASET
            print("Saving Data....")

            df_route = args.data_path + "BP_" + args.method.upper() +"_RPPG2ABP.csv"
            
            df = pd.read_csv(df_route)
            #SBP,DBP,Resolution,FPS,ID
            df = df.append({'DBP':DBP,'SBP':SBP,'Resolution':args.res,'FPS':args.fps,'ID':args.id,},ignore_index=True)
            df.to_csv(df_route,index=False)
            print("Saved....")

        if(args.plot):
            plt.figure(figsize=(30, 15))

            plt.subplot(3, 1, 1)
            plt.plot(time_scale, ppg_signal, c='k', linewidth=2)
            plt.title('Señal rPPG de entrada', fontsize=20)

            plt.subplot(3, 1, 2)
            plt.plot(time_scale, abp_signal_pred_approximate, c='r', linewidth=2)
            plt.ylabel('ABP (mmHg)', fontsize=15)
            plt.title('Salida de la red de aproximación', fontsize=20)

            plt.subplot(3, 1, 3)
            plt.plot(time_scale, abp_signal_pred, c='b', linewidth=2)
            plt.plot(peaks_filtrados * 8.192/1024, abp_signal_pred[:,0][peaks_filtrados], "xr")
            plt.plot(valles_filtrados * 8.192/1024, abp_signal_pred[:,0][valles_filtrados], "xg")
            plt.ylabel('ABP (mmHg)', fontsize=15)
            plt.title('Salida de la red de refinamiento', fontsize=20)


            plt.tight_layout()

            plt.show()
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--method', type=str, help='Selecciona que método a utilizar para la obtención de la señal rPPG (mtts - yarppg)')
    #parse.add_argument('--numtest',type=int, help='Indica el numero de medicion que se esta realizando (1-5)')
    #parser.add_argument('--current_dir', type=str, help='ruta actual donde se encuentra la carpeta del proyecto')
    parser.add_argument('--signal_file', type=str, help= 'Folder where rppg signal is stored.')
    parser.add_argument('--data_path', type=str, default="",help='Folder where dataset is stored. If empty, doesnt save anything.')
    parser.add_argument('--fps',type=int,help='FPS of the original video')
    parser.add_argument('--id',type=int,help='id of the subject')
    parser.add_argument('--res',type=str,default=" ",help="Resolution of the original video.")
    parser.add_argument('--plot',type=bool,default=False,help='Bool for the plot of the signal')
    args = parser.parse_args()

    predict(args)