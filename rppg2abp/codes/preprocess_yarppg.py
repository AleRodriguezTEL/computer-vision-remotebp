import scipy.io
from scipy.signal import butter
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle



path = 'data/yarppg/'
for i in range(1,10):
    nombre = '0'+str(i)+'.csv'
    nombre_destino = '0'+str(i)+'.p'

    #Leer data y obtener 8.192 segundos
    data = pd.read_csv(path + nombre) 
    data_rango = data[(data.ts >= 10) & (data.ts <= 18.192)]
    t1 = data_rango.ts.values - 10
    rppg = data_rango.p0.values

    #upsample 30 -> 125 Hz
    t2 = np.linspace(0, 8.191, 1024)
    upsampled_pulse = np.interp(t2, t1, rppg)

    #filtro pasabanda
    fs = 125
    [b_pulse, a_pulse] = butter(1, [0.75 / fs * 2, 2.5 / fs * 2], btype='bandpass')
    pulse_pred = scipy.signal.filtfilt(b_pulse, a_pulse, np.double(upsampled_pulse))/0.8 +0.5

    #save
    pickle.dump(upsampled_pulse,open(path + nombre_destino,'wb'))

    # #plot
    # plt.subplot(211)
    # plt.plot(upsampled_pulse)
    # plt.title('Señal rPPG obtenida con yarppg + upsampling lineal')
    # plt.subplot(212)
    # plt.plot(pulse_pred)
    # plt.title('Señal filtrada y normalizada')
    # plt.show()