## rPPG2ABP 

Obtención de señal de presión sanguinea (señal continua, estimación de DBP y SBP) a partir de rPPG.

Paper: https://arxiv.org/pdf/2005.01669.pdf 

Repo original: https://github.com/nibtehaz/PPG2ABP 


## Instalación de ambiente:

* Importar el ambiente desde el .yml

```
$ conda env create -f [environment.yml]
```

* Descargar los modelos y ubicarlos en la carpeta "rppg2abp/code/models"

## Ejecución 

1. Predicción de BP a partir de rPPG. Utiliza una señal rPPG de 1024 muestras, 8.192 segundos, 125Hz.

``` 
$ cd codes

$ bash extract_bp @method @signal_path @data_path @res @fps 
```
- method: Especifica el método que se uso para obtener la señal rPPG (mtts o yarppg).  
- signal_path: ruta a la carpeta donde se encuentran las señales guardadas en archivos .p.  
- data_path: ruta a la carpeta donde se encuentran los datasets que guardarán los resultados.
- res: resolution del video original del cual se obtuvo la señal.
- fps: FPS del video original del cual se obtuvo la señal.

## Modelos y otros archivos
Los archivos adicionales se pueden encontrar en este drive:
https://drive.google.com/drive/folders/12Kp15hFE2OwhHELRtYOh8QCM2Je07kVC?usp=sharing

