# PDI_BloodPressure
Proyecto de [ELO328] Procesamiento digital de imágenes sobre el análisis de la presión sanguínea.

# Notas Italo Salgado 2022

## Instalación de ambiente y ejecucción

Instalación sencilla: use virtualenv para crear un ambiente propicio para el programa.
```python
Package             Version
------------------- --------
asgiref             3.4.1
cycler              0.11.0
Django              4.0
djangorestframework 3.13.1
fonttools           4.28.3
joblib              1.1.0
kiwisolver          1.3.2
matplotlib          3.5.0
numpy               1.21.4
opencv-python       4.5.4.60
packaging           21.3
parse               1.19.0
Pillow              8.4.0
pip                 21.3.1
pyparsing           3.0.6
python-dateutil     2.8.2
pytz                2021.3
scikit-learn        1.0.1
scipy               1.7.3
setuptools          60.9.3
setuptools-scm      6.3.2
six                 1.16.0
sklearn             0.0
sqlparse            0.4.2
threadpoolctl       3.0.0
tomli               1.2.2
wheel               0.37.1
```
Puede instalar estas dependencias rapidamente con el requieriments.txt provisto por los desarrolladores.

Para lograr inferencia de los 3 parámetros de interes provistos por el algoritmo se debe ejecutar el archivo main con las dependencias ya configuradas.
```python
python main.py 
```

> Parámetros entregados: Media BPM, Media sistole, Media diastole. 

## Bibliografía:
Nota Alejandro Rodríguez 2022:
 * No se entrego documentación, sólo un manual de usuario que no contiene información relevante.