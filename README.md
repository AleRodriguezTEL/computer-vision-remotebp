# Computer Vision RemoteBP

Computer Vision project for measuring blood pressure remotely

## Contenido

Este git recopila los 3 proyectos que se han utilizado en la investigación de obtención de presión sanguínea de forma remota. Esto incluye:
- Proyecto 1 PDI: PDI Bloodpressure [git original](https://gitlab.com/wildsense/nam-vital-signs/visionpg/-/tree/master/PDI_BloodPreasure)
- yarPPG [git original](https://github.com/SamProell/yarppg)
- MTTS-CAN [git original](https://github.com/xliucs/MTTS-CAN)
- PPG2ABP [git original](https://github.com/nibtehaz/PPG2ABP)
- Assessment of non-invasive blood pressure prediction from PPG and rPPG signals using deep learning [git original](https://github.com/Fabian-Sc85/non-invasive-bp-estimation-using-deep-learning)


## Modelos y otros archivos
Los archivos adicionales se pueden encontrar en este drive:
https://drive.google.com/drive/folders/12Kp15hFE2OwhHELRtYOh8QCM2Je07kVC?usp=sharing

