## rPPG con Deep Learning

Inferencia de valores de presión sanguínea a partir de una señal PPG. Incluye 4 redes neuronales: AlexNet, ResNet, Slapnicar y LSTM.

Paper: https://www.mdpi.com/1424-8220/21/18/6022/htm

Git original: https://github.com/Fabian-Sc85/non-invasive-bp-estimation-using-deep-learning

Instrucciones de ambiente:

Seguir las instrucciones del git original.

Instrucciones para obtener señal rPPG:

0. La señal rppg debe estar guardada en un archivo .p en la carpeta DATA. Esta ruta puede ser cambiada dentro del codigo en el data loader

1. Obtener valores de presión saguínea:
`python rppg_prediction.py --network=alexnet` -> --network selecciona la NN a utilizar: alexnet, resnet, slapnicar o lstm 

2. Los valores se muestran por pantalla. Se comento el código encargado de guardar los valores en un .csv.