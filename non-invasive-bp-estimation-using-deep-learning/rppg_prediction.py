#from tensorflow import keras as ks
import tensorflow.keras as ks
import tensorflow as tf
from kapre import STFT, Magnitude, MagnitudeToDecibel
from os.path import  join
import pickle
import numpy as np
import argparse
import pandas as pd
def predict_vitals(args):

    dependencies = {
            'ReLU': ks.layers.ReLU,
            'STFT': STFT,
            'Magnitude': Magnitude,
            'MagnitudeToDecibel': MagnitudeToDecibel
    }
    #Loads NN model
    nn = args.network
    model = ks.models.load_model("./trained_models/" + nn + "_ppg_nonmixed.h5", custom_objects=dependencies)

    #Load retrain rppg weights
    subdir = "/mnt/HDD/ARV/non-invasive-bp-estimation-using-deep-learning/trained_models/"
    filename= nn + "_retrain_pers__ppg_nonmixed.h5"
    #model.load_weights(join(subdir,filename))


    #model.summary()
    
    #Load rppg signal file
    with open('data/NAM/ppg_prediction_'+str(args.id)+'.p', 'rb') as f:
        x = pickle.load(f)
    

    #Resize signal. 
    #Models inputs must be (1,875,1). 

    x = x[:875]
    x  = np.reshape(x,(1,875,1))


    #Predict
    result = model.predict(np.array(x)) 
    SBP = result[0][0][0]
    DBP = result[1][0][0]
    print("SBP: "+str(SBP))
    print("DBP: "+str(DBP))
    
    #Saving data in the DATASET
    #print("Saving Data....")
    #df_route = "../DATA/BP_Fabian.csv"
    #df = pd.read_csv(df_route)
    #df = df.append({'DBP':DBP,'SBP':SBP,'ID':args.id,'NN':nn, 'Time': args.duration},ignore_index=True)
    #df.to_csv(df_route,index=False)
    #print("Saved....")

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--id', type=int, default=0,help='ID of the subject video')
    parser.add_argument('--network', type=str, help='Neuronal Network to use')
    parser.add_argument('--duration', type=int, default=0,help='Time duration of the samples videos')
    args = parser.parse_args()

    predict_vitals(args)