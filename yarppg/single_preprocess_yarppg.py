import scipy.io
from scipy.signal import butter
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import parser
import argparse
import sys
#dir = os.getcwd()
path = ''
parser = argparse.ArgumentParser(description="Script for filtering the rppg from .csv")
parser.add_argument("--data_path", type=str,
                        help="path to the .csv where data is stored")
parser.add_argument("--id", default=0,type=int,
                        help="Id of the subject")
parser.add_argument("--store_path", default="",type=str,
                        help="path to the folder where data will be stored")
args = parser.parse_args(sys.argv[1:])
#nombre = 'rppg.csv'
nombre_destino = 'ppg_prediction_'+str(args.id)+'.p'

#Leer data y obtener 8.192 segundos
data = pd.read_csv(args.data_path) 
data_rango = data[(data.ts >= 10) & (data.ts <= 18.192)]
t1 = data_rango.ts.values - 10
rppg = data_rango.p0.values

#upsample 30 -> 125 Hz
t2 = np.linspace(0, 8.191, 1024)
upsampled_pulse = np.interp(t2, t1, rppg)

#filtro pasabanda
fs = 125
[b_pulse, a_pulse] = butter(1, [0.75 / fs * 2, 2.5 / fs * 2], btype='bandpass')
pulse_pred = scipy.signal.filtfilt(b_pulse, a_pulse, np.double(upsampled_pulse))/0.8 +0.5

#save
pickle.dump(upsampled_pulse,open(args.store_path + nombre_destino,'wb'))
