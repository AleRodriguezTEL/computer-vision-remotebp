# Computer Vision RemoteBP

Computer Vision project for measuring blood pressure remotely

## yet another rPPG 
Git basado en el repositorio: yarPPG (https://github.com/SamProell/yarppg)


## Instalación

* Crear y activar ambiente virtual

```
$ python -m [venv_name] [path_to_venv_name]
$ source [venv_name]/bin/activate
$ pip install requeriments.txt
```

## Ejecución 

1.- Ejecutar script para la extracción de señales de un conjunto de videos: 

```
$ bash extract_rppg [video_path] [data_path] [roi]

```
- video_path: ruta a la carpeta de videos.  
- data_path: ruta a la carpeta donde se desea guardar el resultado.  
- roi: ROI seleccionado. Véase la documentancion de yarPPG.  


2.- La salida del yarPPG esta en formato .csv. Ejecutar script para hacer el cambio:

```
$ bash csv2picke [csv_path]  [data_path]
```
- csv_path: ruta a la carpeta con los archivos .csv.  
- data_path: ruta a la carpeta donde se desea guardar el resultado. 

3.- Desactivar el ambiente

```
$ deactivate
```


## Modelos y otros archivos
Los archivos adicionales se pueden encontrar en este drive:
https://drive.google.com/drive/folders/12Kp15hFE2OwhHELRtYOh8QCM2Je07kVC?usp=sharing

